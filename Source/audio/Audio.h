/*
  ==============================================================================

    Audio.h
    Created: 10 Dec 2018 12:45:19pm
    Author:  Corey Ford

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

using AudioGraphIOProcessor = AudioProcessorGraph::AudioGraphIOProcessor;

//==============================================================================

class Audio
{
public:
    Audio();
    ~Audio();
private:
    AudioDeviceManager deviceManager; //< our device manager instance
    
    AudioProcessorPlayer player; //< player that pushes audio through the filter graph
    AudioProcessorGraph graph; //< graph of audio processors
};
