/*
  ==============================================================================

    Audio.cpp
    Created: 10 Dec 2018 12:45:19pm
    Author:  Corey Ford

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    deviceManager.initialiseWithDefaultDevices (2, 2);
    
    player.setProcessor (&graph);
    deviceManager.addAudioCallback (&player);
    
    // reset...
    // clear out the processor graph
    graph.clear();
    
    // Create and add new input/output processor nodes.
    AudioGraphIOProcessor* in = new AudioGraphIOProcessor (AudioGraphIOProcessor::audioInputNode);
    
//    fInputNode = AddProcessor(in);
    
    AudioProcessorGraph::AudioGraphIOProcessor* out = new AudioGraphIOProcessor (AudioGraphIOProcessor::audioOutputNode);

    //    fOutputNode = this->AddProcessor(out);
    
//    this->Connect(fInputNode, fOutputNode);
}

Audio::~Audio()
{
    
}
